package com.example.entity;

/**
 * Created by Administrator on 2017-08-31.
 */
public class JbhvBfpdc {
    private String djbh;
    private String shdd;
    private String djlx;
    private String wlbh;
    private float jz;
    private String rkbz;
    private String c1;
    //发货数量
    private float u6;
    //发货块数
    private float u7;
    private float u8;
    //剩余数量
    private float u9;

    public String getDjbh() {
        return djbh;
    }

    public void setDjbh(String djbh) {
        this.djbh = djbh;
    }

    public String getShdd() {
        return shdd;
    }

    public void setShdd(String shdd) {
        this.shdd = shdd;
    }

    public String getDjlx() {
        return djlx;
    }

    public void setDjlx(String djlx) {
        this.djlx = djlx;
    }

    public String getWlbh() {
        return wlbh;
    }

    public void setWlbh(String wlbh) {
        this.wlbh = wlbh;
    }

    public float getJz() {
        return jz;
    }

    public void setJz(float jz) {
        this.jz = jz;
    }

    public String getRkbz() {
        return rkbz;
    }

    public void setRkbz(String rkbz) {
        this.rkbz = rkbz;
    }

    public String getC1() {
        return c1;
    }

    public void setC1(String c1) {
        this.c1 = c1;
    }

    public float getU6() {
        return u6;
    }

    public void setU6(float u6) {
        this.u6 = u6;
    }

    public float getU7() {
        return u7;
    }

    public void setU7(float u7) {
        this.u7 = u7;
    }

    public float getU8() {
        return u8;
    }

    public void setU8(float u8) {
        this.u8 = u8;
    }

    public float getU9() {
        return u9;
    }

    public void setU9(float u9) {
        this.u9 = u9;
    }

    @Override
    public String toString() {
        return "JbhvBfpdc{" +
                "djbh='" + djbh + '\'' +
                ", shdd='" + shdd + '\'' +
                ", djlx='" + djlx + '\'' +
                ", wlbh='" + wlbh + '\'' +
                ", jz=" + jz +
                ", rkbz='" + rkbz + '\'' +
                ", c1='" + c1 + '\'' +
                ", u6='" + u6 + '\'' +
                ", u7='" + u7 + '\'' +
                ", u8='" + u8 + '\'' +
                ", u9='" + u9 + '\'' +
                '}';
    }
}
