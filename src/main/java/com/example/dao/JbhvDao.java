package com.example.dao;

import com.example.entity.JbhvBfpdc;
import com.example.until.DBUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/8/23.
 */
@Repository
public class JbhvDao {
    public static Logger logger= LoggerFactory.getLogger(JbhvDao.class);

    /**
     * 针对金鸿公司。根据单据编号更改入库标志,此方法只能修改条码号，所以单据编号填写的是条码号
     * @param djbh
     * @return int
     */
    public int updatebydjbh4jbhs(String djbh) {
        String sql="update bfpdc set bfpdc_rkbz='1' where bfpdc_djbh='"+djbh+"'";
        System.out.println(sql);
        DBUtil dbUtil= new DBUtil();
        Connection con =dbUtil.getCon();
        PreparedStatement pstt=null;
        int crow=0;
        try {
            pstt=con.prepareStatement(sql);
            crow=pstt.executeUpdate();
            logger.info("单据编号"+djbh+":执行成功"+crow);
            return crow;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * 更改配货单
     * @param startdjbh
     * @param enddjbh
     * @return
     */
    public int updatebydjbh4jbhs(String startdjbh,String enddjbh){
        String tempStr=startdjbh.substring(0,9);
        //20170804
        logger.info(tempStr);
        String startStr=startdjbh.substring(9,startdjbh.length());
        logger.info(startStr);
        String endStr=enddjbh.substring(9,startdjbh.length());
        int endInt=Integer.valueOf(endStr);
        int startInt = Integer.valueOf(startStr);
        DBUtil dbUtil=new DBUtil();
        Connection con = dbUtil.getCon();
        int temp=startdjbh.length()-9;
        logger.info(String.valueOf(temp));
        int crow=0;
        for(int i=startInt;i<=endInt;i++){
            StringBuilder sb=new StringBuilder(tempStr);
            String inum=String.format("%0"+temp+"d",i);
            sb.append(inum);
            String sql = "update bfpdc set bfpdc_rkbz='1',bfpdc_u6=(select bfpdc_jz from bfpdc where bfpdc_djbh='" + sb + "'),bfpdc_u7=(select bfpdc_u1 from bfpdc where bfpdc_djbh='" + sb + "'),bfpdc_u8=(select bfpdc_u2 from bfpdc where bfpdc_djbh='" + sb + "') where bfpdc_djbh='" + sb + "'";
            System.out.println(sql);
            try {
                PreparedStatement pstt = con.prepareStatement(sql);
                crow=crow+pstt.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }



        return crow;
    }



    public Map findbydjbh4jbhs(String startdjbh, String enddjbh){
        String tempStr=startdjbh.substring(0,9);
        //20170804
        logger.info(tempStr);
        String startStr=startdjbh.substring(9,startdjbh.length());
        logger.info(startStr);
        String endStr=enddjbh.substring(9,startdjbh.length());
        int endInt=Integer.valueOf(endStr);
        int startInt = Integer.valueOf(startStr);
        DBUtil dbUtil=new DBUtil();
        Connection con = dbUtil.getCon();
        int temp=startdjbh.length()-9;
        logger.info(String.valueOf(temp));
        int crow=0;
        Map map=new HashMap<>();
        List<JbhvBfpdc> jbhvBfpdcs=new ArrayList<JbhvBfpdc>();
        for(int i=startInt;i<=endInt;i++){
            StringBuilder sb=new StringBuilder(tempStr);
            String inum=String.format("%0"+temp+"d",i);
            sb.append(inum);
            String sql="select * from bfpdc where bfpdc_djbh="+"'"+sb+"'";
logger.info(sql);

            try {
                PreparedStatement pstt = con.prepareStatement(sql);
                ResultSet rs=pstt.executeQuery();
                int j=0;
                while(rs.next()){
                    //司磅地点
                    JbhvBfpdc bfpdc=new JbhvBfpdc();

                    bfpdc.setC1(rs.getString("bfpdc_c1"));
                    bfpdc.setDjbh(rs.getString("bfpdc_djbh"));
                    bfpdc.setWlbh(rs.getString("bfpdc_wlbh"));
                    bfpdc.setDjlx(rs.getString("bfpdc_djlx"));
                    bfpdc.setJz(rs.getFloat("bfpdc_jz"));
                    bfpdc.setRkbz(rs.getString("bfpdc_rkbz"));
                    bfpdc.setShdd(rs.getString("bfpdc_shdd"));
                    bfpdc.setU6(rs.getFloat("bfpdc_u6"));
                    bfpdc.setU7(rs.getFloat("bfpdc_u7"));
                    bfpdc.setU8(rs.getFloat("bfpdc_u8"));
                    bfpdc.setU9(rs.getFloat("bfpdc_u9"));
                    jbhvBfpdcs.add(bfpdc);
                }
                crow++;

            } catch (SQLException e) {
                e.printStackTrace();
            }
            map.put("crow",crow) ;
            map.put("list",jbhvBfpdcs);
            return map;
        }
        return null;
    }

    public static void main(String args[]){
/*        for (int i=3;i<=132;i++){
            StringBuilder sb=new StringBuilder("2O170815");
            String num= String.format("%03d",i);
            sb.append(num);

            System.out.println(sb.toString());
            JbhvDao jbhvDao=new JbhvDao();
            jbhvDao.updatebydjbh4jbhs(sb.toString());
        }
-----------------------------------------------------
*/
        JbhvDao jbhvDao=new JbhvDao();
        jbhvDao.updatebydjbh4jbhs("201708033223","201708033224");

        /*JbhvDao jbhvDao=new JbhvDao();
        Map map=jbhvDao.findbydjbh4jbhs("201708033223","201708033224");
        System.out.println(map.get("crow"));
        List<JbhvBfpdc> JbhvBfpdcs=(ArrayList<JbhvBfpdc>)map.get("list");
        for (JbhvBfpdc jbhvBfpdc: JbhvBfpdcs) {
                System.out.println(jbhvBfpdc.toString());
        }*/

    }
    }
