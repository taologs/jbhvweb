package com.example.until;

import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by Administrator on 2017/8/23.
 */
public class DBUtil {
    private Properties pro;
    private String url;
    private String user;
    private String pass;
    private String driver;
    public static Logger logger= Logger.getLogger(DBUtil.class);
    public DBUtil(){
        loadFile();
    }

    private void loadFile() {
        if(pro==null) {
            pro=new Properties();

            InputStream in = DBUtil.class.getClassLoader().getResourceAsStream("db.properties");

            try {
                pro.load(in);
                url=pro.getProperty("db.url");
                user=pro.getProperty("db.username");
                pass=pro.getProperty("db.password");
                driver=pro.getProperty("db.driver");

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public Connection  getCon(){

        try {
                Class.forName(driver);
 logger.info(driver+"驱动加载成功了");
                return DriverManager.getConnection(url,user,pass);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String args[]){
       DBUtil dbUtil= new DBUtil();
        dbUtil.getCon();
    }
}
